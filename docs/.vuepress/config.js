module.exports = {
    title: 'GitLab ❤️ VuePress ❤️ VCryptokkie',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public'
}